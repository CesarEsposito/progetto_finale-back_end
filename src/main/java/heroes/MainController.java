package heroes;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Controller
@CrossOrigin(value = "http://localhost:4200")
@RequestMapping(path = "/Hero")
public class MainController {

    /**
     * L'annotation @Autowired significa che il bean viene generato automaticamente da Spring,
     * utilizzato per gestire i dati
     */

    @Autowired
    private HeroRepository heroRepository;

    @Autowired
    private AbilityRepository abilityRepository;


    /**
     * L'annotation @PostConstruct significa che il metodo viene eseguito automaticamente da Spring,
     * dopo che la Build e' stata creata e prima che l'applicativa vada a RunTime.
     * Lo utilizzo per creare i dati e inserirli come Defalt Values ne database
     */
    @PostConstruct
    public void defaultHerosWithAbility() {



        /*
         * Creo le Abilita' di default
         */
        Ability abVolo = new Ability("Volo", "nel blu dipinto di blu");
        Ability abFuoco = new Ability("Resistenza al fuoco", "potere antinfortunistico");
        Ability abForza = new Ability("Super Forza", "contro il grasso più ostinato");
        Ability abAgil = new Ability("Super Agilità", "utile per prendere l'autobus");
        Ability abVel = new Ability("Super Velocità", "corri Forest, corri!");
        Ability abRifles = new Ability("Super Riflessi", "per riflette attentamente");
        Ability abSol = new Ability("Super Soldi", "per tutto il resto");
        Ability abRes = new Ability("Resurrezione", "tanta Roba!");
        Ability abFum = new Ability("Effetti fumosi", "e' vietato!");
        Ability abMag = new Ability("Magia", "e' n trucco!");
        Ability abCa = new Ability("SuperCazzola", "per dileguarsi con facilità");

        /*
         * Creo una lista di cortesia per le Abilita' di default
         * e inserisco le Abilita' nella lista
         */
        List<Ability> listAbilityNow = new ArrayList<>(Arrays.asList(abVolo, abForza, abAgil, abVel, abRifles, abFuoco, abFum, abRes, abSol, abCa));

        /*
         * Uso la lista per popolare il db
         * Gestendo le eccezioni che possono essere lanciate
         */
        try {
            for (Ability ability : listAbilityNow) {
                abilityRepository.save(ability);
            }
        } catch (Exception e) {
            System.out.println("________________________________________________________________________________________");
            System.out.println("Caused:");
            System.out.println("---------------------------------Dato già presente--------------------------------------");
            System.out.println(e.fillInStackTrace().getMessage() + "\n");
            System.out.println("----------------------------------------------------------------------------------------");
            e.printStackTrace();
            System.out.println("________________________________________________________________________________________");
        }

        /*
         * Uso la stessa lista per assegnare le abilità a ciascun Eroe
         * e inserire successivamente l'Eroe nel db
         * Gestendo le eccezzioni che posso essere lanciate
         */
        try {

            listAbilityNow.clear();
            listAbilityNow.addAll(Arrays.asList(abVolo, abForza, abAgil, abVel, abRifles, abFuoco));
            Hero superMan = new Hero("Superman", "supermail@hero.com",
                    "https://static.comicvine.com/uploads/original/0/40/2022668-s4.jpg"
                    , listAbilityNow);
            heroRepository.save(superMan);


            listAbilityNow.clear();
            listAbilityNow.addAll(Arrays.asList(abSol, abAgil, abRifles));
            Hero batMan = new Hero("Batman", "batmail@hero.com",
                    "https://www.nerdplanet.it/wp-content/uploads/2018/03/Batman.jpg"
                    , listAbilityNow);
            heroRepository.save(batMan);

            listAbilityNow.clear();
            listAbilityNow.addAll(Arrays.asList(abFuoco, abForza, abVel));
            Hero aquaMan = new Hero("Aquaman ", "acquamail@hero.com",
                    "https://images.everyeye.it/img-singole/articolo-46859.jpg"
                    , listAbilityNow);
            heroRepository.save(aquaMan);


            listAbilityNow.clear();
            listAbilityNow.addAll(Arrays.asList(abAgil, abRifles, abVel));
            Hero catWoman = new Hero("Catwoman ", "catmail@hero.com",
                    "https://www.syfy.com/sites/syfy/files/styles/1200x1200/public/dc_looney_tunes_catwoman_tweet_and_sylvester.jpg?itok=-8wJEUwV&timestamp=1526581441"
                    , listAbilityNow);
            heroRepository.save(catWoman);

            listAbilityNow.clear();
            listAbilityNow.addAll(Arrays.asList(abForza, abAgil, abVel));
            Hero wonderWoman = new Hero("Wonderwoman ", "wondermail@hero.com",
                    "https://i1.wp.com/redcapes.it/wp-content/uploads/2018/04/wonder-woman.jpg?fit=1920%2C1080&ssl=1"
                    , listAbilityNow);
            heroRepository.save(wonderWoman);

            listAbilityNow.clear();
            listAbilityNow.addAll(Arrays.asList(abForza, abAgil, abVel));
            Hero drStrage = new Hero("Dr. Strage", "strangemail@hero.com",
                    "https://scontent-frt3-2.cdninstagram.com/vp/ac7b259af04a563296a40b3bde94f4de/5C51AA74/t51.2885-15/e35/28156215_1786613411642965_45920372989624320_n.jpg"
                    , listAbilityNow);
            heroRepository.save(drStrage);

            listAbilityNow.clear();
            listAbilityNow.addAll(Arrays.asList(abForza, abAgil, abVel));
            Hero thor = new Hero("Thor", "thormail@hero.com",
                    "https://imgc.allpostersimages.com/img/print/posters/marvel-comics-retro-mighty-thor-comic-panel-flying_a-G-13758347-13198931.jpg"
                    , listAbilityNow);
            heroRepository.save(thor);


            listAbilityNow.clear();
            listAbilityNow.addAll(Arrays.asList(abForza, abAgil, abRifles, abVel));
            Hero spiderMan = new Hero("Spiderman ", "spidermail@hero.com",
                    "https://www.mallorca-ok.de/wp-content/uploads/2014/11/Spiderman-Comic.jpg"
                    , listAbilityNow);
            heroRepository.save(spiderMan);


            listAbilityNow.clear();
            listAbilityNow.addAll(Arrays.asList(abAgil, abRifles, abCa));
            Hero batCat = new Hero("BatCat", "batcat@hero.com",
                    "https://cdn.shopify.com/s/files/1/0032/7882/products/batcat2.jpg?v=1314646495"
                    , listAbilityNow);
            heroRepository.save(batCat);


            listAbilityNow.clear();
            listAbilityNow.addAll(Arrays.asList(abAgil, abRifles, abVel));
            Hero spiderPork = new Hero("Spiderpork ", "porkmail@hero.com",
                    "https://i.ytimg.com/vi/01oyI1sInMc/maxresdefault.jpg", listAbilityNow);
            heroRepository.save(spiderPork);


            listAbilityNow.clear();
            listAbilityNow.addAll(Arrays.asList(abVolo, abForza, abVel, abFuoco));
            Hero superDog = new Hero("Superdog", "dogmail@hero.com",
                    "https://i.ytimg.com/vi/54WMrbzoYf4/maxresdefault.jpg"
                    , listAbilityNow);
            heroRepository.save(superDog);


            listAbilityNow.clear();
            listAbilityNow.addAll(Arrays.asList(abAgil, abRifles, abCa));
            Hero ratMan = new Hero("Rat-Man", "ratmail@hero.com",
                    "http://www.studiocampedelli.net/wp-content/uploads/2016/05/Rat-Man-1024x799.jpg"
                    , listAbilityNow);
            heroRepository.save(ratMan);


        } catch (DataIntegrityViolationException e) {
            System.out.println("________________________________________________________________________________________");
            System.out.println("Caused:");
            System.out.println(e.fillInStackTrace().getMessage() + "\n");
            System.out.println("---------------------------------Dato già presente--------------------------------------");
            e.printStackTrace();
            System.out.println("________________________________________________________________________________________");

        }

    }


    /**
     * Inserisce un nuova Eroe
     * L'annotation @ResponseBody significa che l'oggetto sara' tornato nel body.
     * L'annotation @RequestParam significa  che e' un parametro dell richiesta GET orPOST
     */
    @PostMapping(path = "/add")
    public @ResponseBody
    String addNewHero(@RequestParam String name, @RequestParam String email) {

        if (name != null && name.length() > 2) {
            if (email.equals("")) {
                email = null;
            }
            heroRepository.save(new Hero(name, email));
            return "Eroe Salvato";
        } else
            return "Il nome dell'Eroe deve essere composto da almeno tre caratteri";
    }

    /**
     * Ritorna tutti gli Eroi presenti nel db
     */
    @GetMapping(path = "/all")
    public @ResponseBody
    Iterable<Hero> getAllHeroes() {
        return heroRepository.findAll();
    }


    /**
     * Ritorna uno specifico Eroe con i suoi dettagli
     */
    @GetMapping(path = "/detail")
    public @ResponseBody
    Optional<Hero> getHero(@RequestParam long id) {
        return heroRepository.findById(id);
    }

    /**
     * Elimina un Eroe
     */
    @DeleteMapping(path = "/delete")
    public @ResponseBody
    Iterable<Hero> delHero(@RequestParam long id) {
        heroRepository.deleteById(id);
        return heroRepository.findAll();
    }

    /**
     * Modifa un Eroe
     */
    @PutMapping(path = "/update")
    public @ResponseBody
    Hero updateHero(@RequestBody Hero hero) {
        return heroRepository.save(hero);
    }


    /**
     * Ricerca un Eroe
     */
    @GetMapping(path = "/search")
    public @ResponseBody
    Collection<Hero> findHero(@RequestParam String name) {
        return heroRepository.findAll().
                stream().filter(hero -> hero.getName().toLowerCase().contains(name.trim().toLowerCase()))
                .collect(Collectors.toList());
    }

    /**
     * Ritorna tutte le Abilita' presenti nel db
     */
    @GetMapping(path = "/abilities")
    public @ResponseBody
    Iterable<Ability> getAllAbilities() {
        return abilityRepository.findAll();
    }

    /**
     * Ritorna la lista delle Abilita' MANCANTI di uno specifico Eroe
     */
    @GetMapping(path = "/addableAbilities")
    public @ResponseBody
    Iterable<Ability> getAddableAbilities(@RequestParam long idHero) {
        Optional<Hero> h = heroRepository.findById(idHero);
        Hero hero = h.orElse(null);
        List<Ability> presentAbility = Objects.requireNonNull(hero).getListAbility();
        List<Ability> addableAbility = abilityRepository.findAll();
        addableAbility.removeAll(presentAbility);
        return addableAbility;
    }


    /**
     * Creo un metodo di cortesia per trovare un Eroe tramite Id
     */
    private Hero findHerobyIid(long idHero) {
        Optional<Hero> h = heroRepository.findById(idHero);
        return h.orElse(null);
    }

    /**
     * Creo un metodo di cortesia per trovare un'Abilita' tramite Id
     */
    private Ability findAbilityIid(long idAbility) {
        Optional<Ability> a = abilityRepository.findById(idAbility);
        return a.orElse(null);
    }

    /**
     * Ritorna la lista delle Abilita' di uno specifo Eroe
     */
    @GetMapping(path = "/listAbility")
    public @ResponseBody
    Iterable<Ability> getHeroAbilities(@RequestParam long idHero) {
        return findHerobyIid(idHero).getListAbility();
    }

    /**
     * Aggiunge un abilita' ad uno specifico Eroe
     */
    @PostMapping(path = "/addAbilities")
    public @ResponseBody
    List<Ability> addAbilityToHero(@RequestParam long idHero, @RequestParam long idAbility) {

        Ability ability = findAbilityIid(idAbility);
        Hero hero = findHerobyIid(idHero);
        List<Ability> listAbility = hero.getListAbility();

        if (!listAbility.contains(ability)) {
            listAbility.add(ability);
            hero.setListAbility(listAbility);
            heroRepository.save(hero);

            return listAbility;
        } else
            return listAbility;

    }

    /**
     * Elimina un'Abilita'
     */
    @DeleteMapping(path = "/delAbilities")
    public @ResponseBody
    List<Ability> deleteAbilityFromHero(@RequestParam long idHero, @RequestParam long idAbility) {

        Hero hero = findHerobyIid(idHero);
        Ability ability = findAbilityIid(idAbility);
        List<Ability> abilityToMantain = hero.getListAbility();
        abilityToMantain.remove(ability);
        hero.setListAbility(abilityToMantain);
        heroRepository.save(hero);

        return abilityToMantain;


    }

    /**
     * Aggiunge una nuova Abilita' sia all'Eroe che al db
     */
    @PostMapping(path = "/addNewAbility")
    public @ResponseBody
    Ability addNewAbility(@RequestParam long idHero, @RequestParam String ability,
                          @RequestParam String description) {


        if (ability != null && description != null) {
            if (description.equals("")) {
                description = null;
            }
            abilityRepository.save(new Ability(ability, description));

            Optional<Ability> a = abilityRepository.findByAbility(ability);
            Ability ab = a.orElse(null);

            long idAbility = Objects.requireNonNull(ab).getId();
            addAbilityToHero(idHero, idAbility);

            return ab;
        } else
            return null;
    }

    /**
     * Modifica un'Abilita'
     */
    @PutMapping(path = "/modifyAbility")
    public @ResponseBody
    Ability modifyAbility(@RequestParam long idAbility, @RequestParam String ability,
                          @RequestParam String description) {

        Ability abilityToModify = findAbilityIid(idAbility);
        abilityToModify.setAbility(ability);
        abilityToModify.setDescription(description);
        abilityRepository.save(abilityToModify);

        return abilityToModify;
    }

    /**
     * Aggiunge o Modifica l'immagine dell'Eroe
     */
    @PostMapping(path = "/addOrChangeImg")
    public @ResponseBody
    Hero addOrChangeImg(@RequestParam long idHero, @RequestParam String url) {

        Hero hero = findHerobyIid(idHero);
        hero.setPortrait(url);
        heroRepository.save(hero);

        return hero;
    }

    /*______________________________WALLPAPER______________________________________*/

    /**
     * il metodo restituisce un URL random per settare l'immagine
     * di sfondo di hero component
     */
    @GetMapping(path = "/getWall")
    @ResponseBody
    public List<String> defaultWallPapers(@RequestParam int call) {

        String url = null;
        int img;
        if (call <= 0) {
            Random r = new Random();
            img = r.nextInt(8 - 1 + 1) + 1;
        } else img = call;
        switch (img) {
            case 1:
                url = "https://stmed.net/sites/default/files/marvel-comics-wallpapers-27638-8471477.jpg";
                break;
            case 2:
                url = "https://images6.alphacoders.com/618/thumb-1920-618762.jpg";
                break;
            case 3:
                url = "https://wallpaper.wiki/wp-content/uploads/2017/05/Avengers-Vs-X-Men-1920x1080-HD-Wallpaper.jpg";
                break;
            case 4:
                url = "https://stmed.net/sites/default/files/marvel-comics-wallpapers-27638-4667360.jpg";
                break;
            case 5:
                url = "https://stmed.net/sites/default/files/marvel-comics-wallpapers-27638-6021877.jpg";
                break;
            case 6:
                url = "https://i.pinimg.com/originals/1a/a1/be/1aa1be4081039df43b0732dcac42a6e4.jpg";
                break;
            case 7:
                url = "http://www.vexmosaic.com/wp-content/uploads/2015/04/Superheroes.jpg";
                break;
            case 8:
                url = "https://cdn.vox-cdn.com/thumbor/WmL8_VpInoIW7gzRroUp06oiKUI=/0x564:1988x3056/1200x800/filters:focal(861x1516:1179x1834)/cdn.vox-cdn.com/uploads/chorus_image/image/60042713/image1.1528905658.jpeg";
                break;

        }
        return Collections.singletonList(url);
    }

}


