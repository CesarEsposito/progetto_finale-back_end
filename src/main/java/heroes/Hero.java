package heroes;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "heroes")
public class Hero {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "id", nullable = false, unique = true)
    private long id;

    @NotNull
    @Size(min = 3)
    @Column(name = "nome", nullable = false)
    private String name;

    @Email
    @Column(name = "email", unique = true)
    private String email;

    public String getPortrait() {
        return portrait;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    @Column(name = "portrait", unique = true)
    private String portrait;

    @ManyToMany
    @JoinTable(
            name = "HERO_ABI",
            joinColumns = @JoinColumn(name = "HERO_ID", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "ABI_ID", referencedColumnName = "id"))
    private List<Ability> listAbility;

    public Hero() {

    }

    public Hero(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public Hero(String name, String email, String portrait, List listAbility) {
        this.name = name;
        this.email = email;
        this.listAbility = listAbility;
        this.portrait = portrait;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

        /**
         * Il metodo ritorna una lista ordinata per Id
         * Ho preferito implementare il sort nel getter
         * in quanto risulta piu' efficace ed ottimizzato
         * e maggiormente gestibile a livello di codice.
         */
        public List<Ability> getListAbility() {
            listAbility.sort((a1, a2) -> (a1.getId() < a2.getId() ? -1 : (a1 == a2 ? 0 : 1)));
        return listAbility;
    }

    public void setListAbility(List<Ability> listAbility) {
        this.listAbility = listAbility;
    }
}
