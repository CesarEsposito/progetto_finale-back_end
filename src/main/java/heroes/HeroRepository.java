package heroes;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "Hero", path = "Hero")

public interface HeroRepository extends JpaRepository<Hero, Long> {

	Iterable<Hero> findByName(@Param("name") String name);


}
