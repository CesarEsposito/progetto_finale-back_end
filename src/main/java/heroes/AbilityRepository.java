package heroes;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "Ability", path = "Ability")
public interface AbilityRepository extends JpaRepository<Ability, Long> {

	Optional<Ability> findByAbility(@Param("ability") String ability);


}


