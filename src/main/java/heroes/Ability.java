package heroes;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "abilities")
public class Ability {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "ability", nullable = false, unique = true)
    private String ability;

    @Column(name = "description", nullable = false, unique = true)
    String description;

    @ManyToMany(mappedBy = "listAbility")
    List<Hero> heroList;


    public Ability() {
    }

    public Ability(String ability, String description) {
        this.ability = ability;
        this.description = description;
    }


    public long getId() {
        return id;
    }

    public String getAbility() {
        return ability;
    }

    public void setAbility(String ability) {
        this.ability = ability;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }





}


